package test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import algorithm.data.CouplesData;
import algorithm.data.Person;
import algorithm.strategy_pattern.AlgorithmA;
import algorithm.strategy_pattern.AlgorithmB;
import org.junit.Test;
import algorithm.*;

public class FinderTests {
	String sueName = "Sue";
	Date sueBirthDate = new Date(50, 0, 1);
	Person sue = new Person(sueName, sueBirthDate);

	String gregName = "Greg";
	Date gregBirthDate = new Date(52, 5, 1);
	Person greg = new Person(gregName, gregBirthDate);

	String sarahName = "Sarah";
	Date sarahBirthDate = new Date(82, 0, 1);
	Person sarah = new Person(sarahName, sarahBirthDate);

	String mikeName = "Mike";
	Date mikeBirthDate = new Date(79, 0, 1);
	Person mike = new Person(mikeName, mikeBirthDate);

	@Test
	public void Returns_Empty_Results_When_Given_Empty_List() {
		List<Person> userArrayList = new ArrayList<Person>();
		Finder finder = new Finder(userArrayList);
		CouplesData result = finder.Find(new AlgorithmA());
		assertEquals(null, result.partnerOne);
		assertEquals(null, result.partnerTwo);
	}

	@Test
	public void Returns_Empty_Results_When_Given_One_User() {
		List<Person> UserArrayList = new ArrayList<Person>();
		UserArrayList.add(sue);

		Finder finder = new Finder(UserArrayList);

		CouplesData result = finder.Find(new AlgorithmA());

		assertEquals(null, result.partnerOne);
		assertEquals(null, result.partnerTwo);
	}

	@Test
	public void Returns_Closest_Two_For_Two_People() {
		List<Person> userArrayList = new ArrayList<Person>();
		userArrayList.add(sue);
		userArrayList.add(greg);

		System.out.print(userArrayList.get(0));
		Finder finder = new Finder(userArrayList);

		CouplesData result = finder.Find(new AlgorithmA());

		assertEquals(sue, result.partnerOne);
		assertEquals(greg, result.partnerTwo);
	}

	@Test
	public void Returns_Furthest_Two_For_Two_People() {
		List<Person> userArrayList = new ArrayList<Person>();
		userArrayList.add(greg);
		userArrayList.add(mike);

		Finder finder = new Finder(userArrayList);

		CouplesData result = finder.Find(new AlgorithmA());

		assertEquals(greg, result.partnerOne);
		assertEquals(mike, result.partnerTwo);
	}

	@Test
	public void Returns_Furthest_Two_For_Four_People() {
		List<Person> userArrayList = new ArrayList<Person>();
		userArrayList.add(sue);
		userArrayList.add(sarah);
		userArrayList.add(mike);
		userArrayList.add(greg);

		Finder finder = new Finder(userArrayList);

		CouplesData result = finder.Find(new AlgorithmB());

		assertEquals(sue, result.partnerOne);
		assertEquals(sarah, result.partnerTwo);
	}

	@Test
	public void Returns_Closest_Two_For_Four_People() {
		List<Person> userArrayList = new ArrayList<Person>();
		userArrayList.add(sue);
		userArrayList.add(sarah);
		userArrayList.add(mike);
		userArrayList.add(greg);

		Finder finder = new Finder(userArrayList);

		CouplesData result = finder.Find(new AlgorithmA());

		assertEquals(sue, result.partnerOne);
		assertEquals(greg, result.partnerTwo);
	}
}
