package algorithm.data;

import java.util.Date;

public class Person {
	private String _personName; // Person Name
	private Date _personBirth;  // Person Birth Date

	/**
	 * constructor User: Create a new User.
	 * @param name
	 * @param birth
	 */
	public Person(String name, Date birth) {
		this._personName = name;
		this._personBirth = birth;
	}

	/**
	 * getName: Get User Name
	 * @return String Name
	 */
	public String getName() {
		return _personName;
	}

	/**
	 * getBirth: Get User Birth Date
	 * @return Date Birth
	 */
	public Date getBirth() {
		return _personBirth;
	}

	/**
	 * setName: Set User Name
	 * @param name String
	 */
	public void setName(String name) {
		this._personName = name;
	}

	/**
	 * setBirth: Set User Birth
	 * @param birth
	 */
	public void setBirth(Date birth) {
		this._personBirth = birth;
	}
}