package algorithm;

// Current Java Imports
import algorithm.data.CouplesData;
import algorithm.data.Person;
import algorithm.strategy_pattern.Algorithm;
import algorithm.strategy_pattern.ContextSearch;

import java.util.ArrayList;
import java.util.List;

public class Finder {
	public final List<Person> _peopleList;

	/**
	 * Finder (Constructor): Setup list of people to be searched on
	 * @param peopleList
	 */
	public Finder(List<Person> peopleList) {
		_peopleList = peopleList;
	}

	/**
	 * buildCouplesData: This function will return the data structure for the current pairs of people and their ageGap
	 * @return
	 */
	public List<CouplesData> buildCouplesData() {
		List<CouplesData> _newCouplesDataArray = new ArrayList<CouplesData>();

		for (int i = 0; i < this._peopleList.size() - 1; i++) {
			for (int j = i + 1; j < this._peopleList.size(); j++) {
				CouplesData _newCoupleData = new CouplesData();
				if (_peopleList.get(i).getBirth().getTime() < _peopleList.get(j).getBirth().getTime()) {
					_newCoupleData.partnerOne = this._peopleList.get(i);
					_newCoupleData.partnerTwo = this._peopleList.get(j);
				} else {
					_newCoupleData.partnerOne = this._peopleList.get(j);
					_newCoupleData.partnerTwo = this._peopleList.get(i);
				}
				_newCoupleData.ageGap = _newCoupleData.partnerTwo.getBirth().getTime() - _newCoupleData.partnerOne.getBirth().getTime();
				_newCouplesDataArray.add(_newCoupleData);
			}
		}
		return _newCouplesDataArray;
	}

	/**
	 * Find: This function will return the result of applying a specific strategy
	 * @param _algorithm
	 * @return
	 */
	public CouplesData Find(Algorithm _algorithm) {
		List<CouplesData> CouplesDataArray = this.buildCouplesData();
		if (CouplesDataArray.size() < 1) {
			return new CouplesData();
		}

		// Init the ContextSearch Class to define the current strategy to be used in the Finder Class
		ContextSearch _contextSearch = new ContextSearch();
		_contextSearch.setAlgorithm(_algorithm);

		// For each pair of poeple within the list, apply the algorithm and if the current AgeGap it's higher or lower
		// than the last, depending on the selected algorithm, save the value.
		CouplesData answer = CouplesDataArray.get(0);
		for (CouplesData result : CouplesDataArray) {
			if (_contextSearch.executeStrategy(result.ageGap, answer.ageGap)) {
				answer = result;
			}
		}
		return answer;
	}
}
