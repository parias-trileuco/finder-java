package algorithm.strategy_pattern;

public class ContextSearch {
    private Algorithm _algorithm;

    /**
     * setAlgorithm: This function will select the current algorithm to be used as a strategy for the Finder.
     * @param _algorithm
     */
    public void setAlgorithm(Algorithm _algorithm){
        this._algorithm = _algorithm;
    }

    /**
     * executeStrategy: This function will perform the algorithm to be used as a strategy for the Finder.
     * @param a age for Partner1
     * @param b age for Partner2
     * @return
     */
    public boolean executeStrategy(long a, long b){
        return this._algorithm.execute(a, b);
    }
}