package algorithm.strategy_pattern;

public class AlgorithmB implements Algorithm {
    /**
     * execute (AlgorithmB): This function will return if ageGapOne it's "gt" ageGapTwo
     * @param ageGapOne
     * @param ageGapTwo
     * @return
     */
    @Override
    public boolean execute(long ageGapOne, long ageGapTwo) {
        if (ageGapOne > ageGapTwo) {
            System.out.print("Executing strategy B");
            return true;
        }
        return false;
    }
}

