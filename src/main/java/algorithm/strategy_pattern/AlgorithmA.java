package algorithm.strategy_pattern;

public class AlgorithmA implements Algorithm {
    /**
     * execute (AlgorithmA): This function will return if ageGapOne it's "lt" ageGapTwo
     * @param ageGapOne
     * @param ageGapTwo
     * @return
     */
    @Override
    public boolean execute(long ageGapOne, long ageGapTwo) {
        if (ageGapOne < ageGapTwo) {
            System.out.print("Executing strategy A");
            return true;
        }
        return false;
    }
}
