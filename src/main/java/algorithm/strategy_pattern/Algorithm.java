package algorithm.strategy_pattern;

// source: https://stackoverflow.com/questions/24435126/strategy-pattern-using-enums-need-a-simple-example-in-java
// source: https://dzone.com/articles/strategy-pattern-implemented
// source: https://blog.scottlogic.com/2016/07/28/java-enums-how-to-use-them-smarter.html

public interface Algorithm {
	boolean execute(long a, long b);
}